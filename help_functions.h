#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H

#include "hashtable.h"
#include "heap.h"
#include "list.h"

int convertStrToInt(const char*);
double convertStrToDouble(const char*);
bool isTime(const char*);
char* getArgument(int, char*[], const char*);
void readConfFile(double[][5], const char*);
void applyOperationsFromFile(HashTable&, HashTable&, Heap&, double[][5], const char*);
void applyOperations(HashTable&, HashTable&, Heap&, double[][5], const char*);
void insert(HashTable&, HashTable&, Heap&, double[][5], const char*);
void deletes(HashTable&, const char*);
void find(const HashTable&, const char*, int);
void topDest(const HashTable&, const char*);
void topPercent(Heap&, const char*);
void indist(const HashTable&, const HashTable&, const char*);
void bye(HashTable&, HashTable&);
void printHashTable(const HashTable&, const HashTable&, const char*);
void writeHashTable(const HashTable&, const HashTable&, const char*);

#endif
