#ifndef RECORD_H
#define RECORD_H

#include "info.h"
#include "list.h"

class Record
{
    private:
        Info **info;
        Record *next;
        int pos, length, type;

    public:
        Record(int, int);
        ~Record();
        void insert(Info*);
        void deletes(const char*);
        Info* getLast();
        void deleteLast();
        void find(const char*, const char*, const char*, const char*, List *li = NULL, int flag = -1) const;
        void topDest() const;
        bool exist(const char*) const;
        void printByPos(int) const;
        void printRecords(FILE *file = stdout) const;
        void clear();
};

#endif
