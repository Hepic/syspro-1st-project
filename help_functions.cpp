#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "help_functions.h"

int convertStrToInt(const char *str)
{
    int ret = 0;

    while(*str != '\0')
        ret = ret * 10 + *str++ - '0';
    
    return ret;
}

double convertStrToDouble(const char *str)
{
    double ret = 0, pow = 0.1;
    bool dot = false;
    
    while(*str != '\0')
    {
        if(*str == '.')
        {
            ++str;
            dot = true;
            continue;
        }
        
        if(dot)
        {
            ret += (*str++ - '0') * pow;
            pow /= 10;
        }
        
        else
            ret = ret * 10 + *str++ - '0';
    }

    return ret;
}

bool isTime(const char *str)
{
    while(*str != '\0')
        if(*str++ == ':')
            return true;
    
    return false;
}

char* getArgument(int len, char *seq[], const char *flag)
{
    int pos = -1;

    for(int i=0; i<len; ++i)
        if(!strcmp(seq[i], flag))
        {
            pos = i + 1;
            break;
        }
    
    if(pos == -1)
        return NULL;

    return seq[pos];
}

void readConfFile(double costs[][5], const char *file_name)
{
    FILE *file = fopen(file_name, "r");
    char buff[255];
    
    while(fgets(buff, sizeof(buff), file))
    {
        buff[strlen(buff) - 1] = '\0'; 
        
        if(buff[0] == '#')
            continue;

        char *ptr = strtok(buff, ";");
        int cnt = 1, type, tarrif;
        double cost;

        while(ptr != NULL)
        {
            switch(cnt)
            {
                case 1:
                    type = convertStrToInt(ptr);
                break;

                case 2:
                    tarrif = convertStrToInt(ptr);
                break;

                case 3:
                    cost = convertStrToDouble(ptr);
                break;
            }
            
            ptr = strtok(NULL, ";");
            ++cnt;
        }
        
        costs[type][tarrif] = cost; // save its combination
    }

    fclose(file);
}

void applyOperationsFromFile(HashTable &caller, HashTable &callee, Heap &heap, double costs[][5], const char *file_name)
{
    FILE *file = fopen(file_name, "r");
    char buff[255], oper[255];
    
    while(fgets(buff, sizeof(buff), file))
    {
        buff[strlen(buff) - 1] = '\0';
        applyOperations(caller, callee, heap, costs, buff); 
    }

    fclose(file);
}

void applyOperations(HashTable &caller, HashTable &callee, Heap &heap, double costs[][5], const char *buff)
{
    char oper[255];
    memset(oper, 0, sizeof(oper));
    
    for(int i=0; i<strlen(buff); ++i)
    {
        if(buff[i] == ' ')
            break;

        oper[i] = buff[i];
    }

    if(!strcmp(oper, "insert"))
        insert(caller, callee, heap, costs, buff+strlen(oper)+1);

    else if(!strcmp(oper, "delete"))
        deletes(caller, buff+strlen(oper)+1);
    
    else if(!strcmp(oper, "find"))
        find(caller, buff+strlen(oper)+1, 1);
    
    else if(!strcmp(oper, "lookup"))
        find(callee, buff+strlen(oper)+1, 2);

    else if(!strcmp(oper, "topdest"))
        topDest(caller, buff+strlen(oper)+1);
    
    else if(!strcmp(oper, "top"))
        topPercent(heap, buff+strlen(oper)+1);

    else if(!strcmp(oper, "indist"))
        indist(caller, callee, buff+strlen(oper)+1);

    else if(!strcmp(oper, "bye"))
        bye(caller, callee);
    
    else if(!strcmp(oper, "print"))
        printHashTable(caller, callee, buff+strlen(oper)+1);
    
    else if(!strcmp(oper, "dump"))
        writeHashTable(caller, callee, buff+strlen(oper)+1);
}

void insert(HashTable &caller, HashTable &callee, Heap &heap, double costs[][5], const char *input)
{
    printf("insert >>> %s\n", input);

    char *copy = new char[strlen(input) + 1];
    strcpy(copy, input);

    char *ptr = strtok(copy, ";");
    int cnt = 1;

    char *originator, *destination;
    int type, tarrif, duration, fault;
    double cost;

    while(ptr != NULL)
    {
        switch(cnt)
        {
            case 2:
                originator = new char[strlen(ptr) + 1];
                strcpy(originator, ptr);
            break; 

            case 3:
                destination = new char[strlen(ptr) + 1];
                strcpy(destination, ptr);
            break;

            case 6:
                duration = convertStrToInt(ptr);
            break;

            case 7:
                type = convertStrToInt(ptr);
            break;

            case 8:
                tarrif = convertStrToInt(ptr);
            break;

            case 9:
                fault = convertStrToInt(ptr);
            break;
        }

        ptr = strtok(NULL, ";");
        ++cnt;
    }
    
    Info *info = new Info();
    info->insert(input);
    
    caller.insert(info, originator);
    callee.insert(info, destination); 
    
    cost = (costs[type][tarrif]) * (double)duration;
    
    if(!type  &&  !tarrif) // SMS - fixed price
        cost = costs[type][tarrif];
    
    if(fault >= 200  &&  fault <= 299)
        heap.insert(cost, originator);

    printf("------------------------------------------------------------------\n");

    delete[] copy;
    delete[] originator;
    delete[] destination;
}

void deletes(HashTable &caller, const char *input)
{
    printf("delete >>> %s\n", input);
    
    char *copy = new char[strlen(input) + 1];
    strcpy(copy, input);

    char *ptr = strtok(copy, " ");
    int cnt = 1;

    char *cdr, *originator;

    while(ptr != NULL  &&  cnt <= 2)
    {
        switch(cnt)
        {
            case 1:
                originator = new char[strlen(ptr) + 1];
                strcpy(originator, ptr); 
            break;

            case 2:
                cdr = new char[strlen(ptr) + 1];
                strcpy(cdr, ptr);
            break;
        }

        ptr = strtok(NULL, " ");
        ++cnt;
    }
    
    caller.deletes(cdr, originator);
    printf("------------------------------------------------------------------\n");

    delete[] copy;
    delete[] originator;
    delete[] cdr;
}

void find(const HashTable &caller, const char *input, int flag)
{
    if(flag == 1)
        printf("find >>> %s\n", input);
    
    else if(flag == 2)
        printf("lookup >>> %s\n", input);
    
    char *copy = new char[strlen(input) + 1];
    strcpy(copy, input);
    
    char *ptr = strtok(copy, " ");
    int time_cnt = 0, date_cnt = 0;
    bool found_phone = false;

    char *originator, *time1 = NULL, *date1 = NULL, *time2 = NULL, *date2 = NULL;
    
    while(ptr != NULL)
    {
        if(!found_phone)
        {
            originator = new char[strlen(ptr) + 1];
            strcpy(originator, ptr);
            
            found_phone = true;
        }
        
        else if(isTime(ptr))
        {
            if(!time_cnt)
            {
                time1 = new char[strlen(ptr) + 1];
                strcpy(time1, ptr);
            }

            else
            {
                time2 = new char[strlen(ptr) + 1];
                strcpy(time2, ptr);
            }

            ++time_cnt;
        }

        else
        {
            if(!date_cnt)    
            {
                date1 = new char[strlen(ptr) + 1];
                strcpy(date1, ptr); 
            }

            else
            {
                date2 = new char[strlen(ptr) + 1];
                strcpy(date2, ptr);
            }

            ++date_cnt;
        }
        
        ptr = strtok(NULL, " ");
    }
   
    caller.find(originator, time1, date1, time2, date2, NULL, -1);
    printf("------------------------------------------------------------------\n");
    
    delete[] copy;
    delete[] originator;

    if(time1 != NULL)
        delete[] time1;

    if(date1 != NULL)
        delete[] date1;

    if(time2 != NULL)
        delete[] time2;

    if(date2 != NULL)
        delete[] date2;
}

void topDest(const HashTable &caller, const char *input)
{
    printf("topdest of >>> %s\n", input);
    caller.topDest(input);
    printf("------------------------------------------------------------------\n");
}

void topPercent(Heap &heap, const char *input)
{
    printf("top >>> %s%%\n", input);
    
    int value = convertStrToInt(input);
    heap.topPercent(value);
    
    printf("------------------------------------------------------------------\n");
}

void indist(const HashTable &caller, const HashTable &callee, const char *input)
{
    printf("indist >>> %s\n", input);

    char *copy = new char[strlen(input) + 1];
    strcpy(copy, input);
    
    char *ptr = strtok(copy, " ");
    int cnt = 1;
    
    char *caller1, *caller2;

    while(ptr != NULL)
    {
        switch(cnt)
        {
            case 1:
                caller1 = new char[strlen(ptr) + 1];
                strcpy(caller1, ptr);
            break;
            
            case 2:
                caller2 = new char[strlen(ptr) + 1];
                strcpy(caller2, ptr);
            break;
        }

        ptr = strtok(NULL, " ");
        ++cnt;
    }
    
    List li_1, li_2, li;
    
    caller.find(caller1, NULL, NULL, NULL, NULL, &li_1, 1); 
    callee.find(caller1, NULL, NULL, NULL, NULL, &li_1, 2);
    caller.find(caller2, NULL, NULL, NULL, NULL, &li_2, 1);
    callee.find(caller2, NULL, NULL, NULL, NULL, &li_2, 2);
    
    li.intersection(li_1, li_2);

    li.deleteKnown(callee);
    li.print();
    
    printf("------------------------------------------------------------------\n");
    
    delete[] copy;
    delete[] caller1;
    delete[] caller2;
}

void bye(HashTable &caller, HashTable &callee)
{
    caller.clear();
    callee.clear();
}

void printHashTable(const HashTable &caller, const HashTable &callee, const char *input)
{
    if(!strcmp("hashtable1", input))
    {
        printf("print hashtable1 >>> \n");
        caller.printHashTable();
    }

    else if(!strcmp("hashtable2", input))
    {
        printf("print hashtable2 >>> \n");
        callee.printHashTable();
    }

    else
        printf("Invalid input! Enter hashtable1 or hashtable2\n");

    printf("------------------------------------------------------------------\n");
}

void writeHashTable(const HashTable &caller, const HashTable &callee, const char *input)
{
    char *copy = new char[strlen(input) + 1];
    strcpy(copy, input);

    char *ptr = strtok(copy, " ");
    int cnt = 1;
    
    char *hashtable, *file_name;

    while(ptr != NULL)
    {
        switch(cnt)
        {
            case 1:
                hashtable = new char[strlen(ptr) - 1];
                strcpy(hashtable, ptr);
            break;

            case 2:
                file_name = new char[strlen(ptr) - 1];
                strcpy(file_name, ptr);
            break;
        }
        
        ptr = strtok(NULL, " ");
        ++cnt;
    }
    
    FILE *file = fopen(file_name, "w");

    if(!strcmp("hashtable1", hashtable))
    {
        printf("write hashtable1 to %s >>> \n", file_name);
        caller.printHashTable(file);
    }

    else if(!strcmp("hashtable2", hashtable))
    {
        printf("write hashtable2 to %s >>> \n", file_name);
        callee.printHashTable(file);
    }

    else
        printf("Invalid input! Enter hashtable1 or hashtable2\n");
    
    fclose(file);

    delete[] copy;
    delete[] hashtable;
    delete[] file_name;
}
