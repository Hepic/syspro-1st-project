#ifndef LIST_H
#define LIST_H

#include "node.h"

class HashTable;

class List
{
    private:
        Node *head, *tail;

    public:
        List();
        ~List();
        void insert(char*);
        void deleteKnown(const HashTable&);
        bool exist(const char*) const;
        void print() const;
        void intersection(const List&, const List&);
};

#endif
