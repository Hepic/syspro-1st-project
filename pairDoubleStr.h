#ifndef PAIRDOUBLESTR_H
#define PAIRDOUBLESTR_H

class PairDoubleStr
{
    private:
        double val;
        char *word;

    public:
        PairDoubleStr();
        ~PairDoubleStr();
        void insert(double, char*);
        double getDouble() const;
        char *getStr() const;
};

#endif
