#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include "heap.h"

Heap::Heap()
{
    head = NULL;
    length = 0;
}

Heap::~Heap()
{
    delete head;
}

Node* Heap::addNode(double value, char *phone)
{  
    int pos = 0, temp = length;
    
    while(temp)
    {
        ++pos;
        temp = (temp - 1) >> 1;
    }

    int *road = new int[pos + 5];
    temp = length, pos = 0;

    while(temp)
    {
        road[pos++] = temp;
        temp = (temp - 1) >> 1;
    }

    road[pos] = 0; // 'road' keeps the road that we need to follow to find the next position in the heap
    Node *node = head;
    
    if(!length)
    {
        head = new Node(1);
        head->insert(value, phone);
        
        delete[] road;
        return head;
    }

    while(1)
    {
        if(pos > 0  &&  2*road[pos]+1 == road[pos-1]  &&  node->getLeft() != NULL)
            node = node->getLeft();

        else if(pos > 0  &&  2*road[pos]+2 == road[pos-1]  &&  node->getRight() != NULL)
            node = node->getRight();
        
        else
            break;

        --pos;
    }   
   
    Node *newNode = new Node(1);
    newNode->insert(value, phone);
    newNode->setParent(node);
    
    if(pos > 0  &&  2*road[pos]+1 == road[pos-1])
        node->setLeft(newNode);

    else if(pos > 0  &&  2*road[pos]+2 == road[pos-1])
        node->setRight(newNode);
    
    delete[] road;
    return newNode;
}

void Heap::insert(double value, char *phone)
{   
    Node *node = update(head, value, phone); // if that phone already exists in the heap then we update it
    
    if(node == NULL)
    {
        node = addNode(value, phone);
        ++length;
    }

    // after we add the new node we need to place it in the correct position
    while(node->getParent() != NULL)
    {
        double parent_value = node->getParent()->getValue();
        char *parent_phone = node->getParent()->getPhone();

        if(parent_value < node->getValue()) // swap current node with its parent
        {
            double temp_val = node->getValue();
            char *temp_phone = node->getPhone();
            
            node->sets(parent_value, parent_phone);
            node = node->getParent();
            node->sets(temp_val, temp_phone);
        }

        else
            break;
    }
}

Node* Heap::update(Node *node, double value, char *phone)
{
    if(node == NULL)
        return NULL;

    if(!strcmp(node->getPhone(), phone))
    {
        node->addValue(value);
        return node;
    }

    update(node->getRight(), value, phone);
    update(node->getLeft(), value, phone);
}

Node* Heap::getTop() const
{
    return head; 
}

PairDoubleStr Heap::deleteLastNode()
{
    int pos = 0, temp = length - 1;
    
    while(temp)
    {
        ++pos;
        temp = (temp - 1) >> 1;
    }

    int *road = new int[pos + 5];
    temp = length - 1, pos = 0;

    while(temp)
    {
        road[pos++] = temp;
        temp = (temp - 1) >> 1;
    }

    road[pos] = 0; // 'road' keeps the road that we need to follow so as to find the 'last' node of the heap 
    Node *node = head; 

    while(1)
    {
        if(pos > 1  &&  2*road[pos]+1 == road[pos-1]  &&  node->getLeft() != NULL)
            node = node->getLeft();

        else if(pos > 1  &&  2*road[pos]+2 == road[pos-1]  &&  node->getRight() != NULL)
            node = node->getRight();
        
        else
            break;

        --pos;
    }
    
    PairDoubleStr ret;
    
    if(pos > 0  &&  2*road[pos]+1 == road[pos-1])
    {
        ret.insert(node->getLeft()->getValue(), node->getLeft()->getPhone());

        delete node->getLeft();
        node->setLeft(NULL);
    }

    else if(pos > 0  &&  2*road[pos]+2 == road[pos-1])
    {
        ret.insert(node->getLeft()->getValue(), node->getLeft()->getPhone());
     
        delete node->getRight();
        node->setRight(NULL);
    }
    
    delete[] road;
    return ret;
}

void Heap::popTop()
{
    PairDoubleStr ret = deleteLastNode();
    head->insert(ret.getDouble(), ret.getStr()); // replace last node with the head
     
    --length;
    Node* node = head;
    
    // try to find the correct position of the 'last node'
    while(node->getLeft() != NULL  ||  node->getRight() != NULL)
    {
        if(node->getLeft() != NULL  &&  node->getLeft()->getValue() > node->getValue()  &&  
        (node->getRight() == NULL  || (node->getLeft()->getValue() > node->getRight()->getValue())))
        {
            double temp_value = node->getValue();
            char *temp_phone = node->getPhone();

            node->sets(node->getLeft()->getValue(), node->getLeft()->getPhone());
            node = node->getLeft();
            node->sets(temp_value, temp_phone);
        }

        else if(node->getRight() != NULL  &&  node->getRight()->getValue() > node->getValue()  &&
        (node->getLeft() == NULL  ||  (node->getRight()->getValue() > node->getLeft()->getValue())))
        {
            double temp_value = node->getValue();
            char *temp_phone = node->getPhone();

            node->sets(node->getRight()->getValue(), node->getRight()->getPhone());
            node = node->getRight();
            node->sets(temp_value, temp_phone);
        }

        else
            break;
    }
}

void Heap::topPercent(int perc)
{
    int num = (int)round(((double)perc / 100.0) * (double)length);
    PairDoubleStr *result = new PairDoubleStr[num];
    
    for(int i=0; i<num; ++i)
    {
        result[i].insert(getTop()->getValue(), getTop()->getPhone());
        popTop();

        printf("%lf %s\n", result[i].getDouble(), result[i].getStr());
    }

    for(int i=0; i<num; ++i)
        insert(result[i].getDouble(), result[i].getStr());

    delete[] result;
}
