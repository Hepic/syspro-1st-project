#ifndef INFO_H
#define INFO_H

#include "date.h"
#include "time.h"

class Info
{
    private:
        char *cdr;
        char originator[15], destination[15];
        Date *date;
        Time *time;
        int duration, type, tarrif, fault;

    public:
        Info();
        ~Info();
        void insert(const char *);
        void print(FILE *file = stdout) const;
        bool equals(const char*) const;
        bool intoBounds(const char *, const char *, const char *, const char *) const;
        char* getDestination() const;
        char* getOriginator() const;
        int getDestInt() const;
        char* getDestStr() const;
};

#endif
