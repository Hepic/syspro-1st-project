#ifndef TIME_H
#define TIME_H

class Time
{
    private:
        int hour, minute;

    public:
        int getHour() const;
        int getMinute() const;
        void insert(const char*);
        char* convertTimeIntToTimeStr() const; 
};

#endif
