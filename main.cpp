#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "hashtable.h"
#include "heap.h"
#include "help_functions.h"

using namespace std;


int main(int argc, char *argv[])
{
    char *operation_file = getArgument(argc, argv, "-o");
    int hashTable1_len = convertStrToInt(getArgument(argc, argv, "-h1"));
    int hashTable2_len = convertStrToInt(getArgument(argc, argv, "-h2"));
    int bucket_size = convertStrToInt(getArgument(argc, argv, "-s"));
    char *conf_file = getArgument(argc, argv, "-c");
    
    HashTable caller(hashTable1_len, bucket_size, 1), callee(hashTable2_len, bucket_size, 2);
    Heap heap;
    double costs[5][5];
    
    readConfFile(costs, conf_file);
    
    if(operation_file != NULL)
        applyOperationsFromFile(caller, callee, heap, costs, operation_file);
    
    char input[255];
    printf("Enter something: \n");
    
    while(fgets(input, sizeof(input), stdin))
    {
        input[strlen(input) - 1] = '\0';
        applyOperations(caller, callee, heap, costs, input);

        printf("Enter something: \n");
    }
    
    return 0;
}

