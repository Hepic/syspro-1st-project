#include <cstdio>
#include <cstring>
#include "info.h"
#include "help_functions.h"

Info::Info()
{
    cdr = NULL;
}

Info::~Info()
{
    if(cdr != NULL)
        delete[] cdr;

    delete time;
    delete date;
}

void Info::insert(const char *value)
{
    char *copy = new char[strlen(value) + 1];
    strcpy(copy, value);

    char *ptr = strtok(copy, ";");
    int cnt = 1;

    while(ptr != NULL)
    {
        switch(cnt)
        {
            case 1:
               cdr = new char[strlen(ptr) + 1];
               strcpy(cdr, ptr);
            break;

            case 2:
                strcpy(originator, ptr);
            break;

            case 3:
                strcpy(destination, ptr);
            break;

            case 4:
                date = new Date();
                date->insert(ptr);
            break;
            
            case 5:
                time = new Time();
                time->insert(ptr); 
            break;

            case 6:
                duration = convertStrToInt(ptr);
            break;

            case 7:
                type = convertStrToInt(ptr);
            break;

            case 8:
                tarrif = convertStrToInt(ptr);
            break;

            case 9:
                fault = convertStrToInt(ptr);
            break;
        }

        ptr = strtok(NULL, ";");
        ++cnt;
    }

    delete[] copy;
}

void Info::print(FILE *file) const
{
    fprintf(file, "\nCDR-ID = %s\n", cdr);
    fprintf(file, "Originator-Number = %s\n", originator);
    fprintf(file, "Destination-Number = %s\n", destination);
    fprintf(file, "Date = %d:%d:%d\n", date->getDay(), date->getMonth(), date->getYear());
    fprintf(file, "Time = %d:%d\n", time->getHour(), time->getMinute());
    fprintf(file, "Duration = %d\n", duration);
    fprintf(file, "Type = %d\n", type);
    fprintf(file, "Tarrif = %d\n", tarrif);
    fprintf(file, "Fault = %d\n", fault);
}

bool Info::equals(const char *cdr) const
{
    return !strcmp(cdr, this->cdr);
}

bool Info::intoBounds(const char *time1, const char *date1, const char *time2, const char *date2) const
{
    char *time_str = time->convertTimeIntToTimeStr();
    char *date_str = date->convertDateIntToDateStr();
    
    char rev_date1[11];
    char rev_date2[11];
    
    if(date1 != NULL  &&  date2 != NULL)
    {
        for(int i=0; i<4; ++i)
        {
            rev_date1[i] = date1[i+4];
            rev_date2[i] = date2[i+4];
        }

        for(int i=0; i<2; ++i)
        {
            rev_date1[i+4] = date1[i+2];
            rev_date1[i+6] = date1[i];

            rev_date2[i+4] = date2[i+2];
            rev_date2[i+6] = date2[i];
        }
    }
    
    // 'whole ith' keeps the format of date/time in that way so we can compare them
    char whole_1[20];
    char whole_2[20];
    char whole[20];
    
    memset(whole_1, 0, sizeof(whole_1));
    memset(whole_2, 0, sizeof(whole_1));
    memset(whole, 0, sizeof(whole));
    
    int p = 0, p1 = 0, p2 = 0;
    
    if(date1 != NULL  &&  date2 != NULL)
    {
        for(int i=0; i<strlen(date_str); ++i)
        {
            whole[p++] = date_str[i];
            whole_1[p1++] = rev_date1[i];
            whole_2[p2++] = rev_date2[i];
        }
    }

    whole[p++] = whole_1[p1++] = whole_2[p2++] = '|';
    
    if(time1 != NULL  &&  time2 != NULL)
    {
        for(int i=0; i<strlen(time_str); ++i)
        {
            whole[p++] = time_str[i];
            whole_1[p1++] = time1[i];
            whole_2[p2++] = time2[i];
        }
    }
    
    delete[] time_str;
    delete[] date_str;
    
    return strcmp(whole_1, whole) <= 0  &&  strcmp(whole, whole_2) <= 0;
}

char* Info::getDestination() const
{
    char *copy = new char[strlen(destination) + 1];
    strcpy(copy, destination);
    
    return copy;
}

char* Info::getOriginator() const
{
    char *copy = new char[strlen(originator) + 1];
    strcpy(copy, originator);
    
    return copy;
}

int Info::getDestInt() const
{
    int ret = 0;
    
    for(int i=0; i<3; ++i)
        ret = ret*10 + destination[i] - '0';

    return ret;
}

char* Info::getDestStr() const
{
    char *ret = new char[3];
    
    for(int i=0; i<3; ++i)
        ret[i] = destination[i];

    return ret;
}
