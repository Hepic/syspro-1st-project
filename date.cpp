#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "date.h"
#include "help_functions.h"

int Date::getDay() const
{
    return day;
}

int Date::getMonth() const
{
    return month;
}

int Date::getYear() const
{
    return year;
}

void Date::insert(const char *str)
{
    char copy[6];
    memset(copy, 0, sizeof(copy));

    for(int i=0; i<2; ++i)
        copy[i] = str[i];
    
    day = convertStrToInt(copy);
    
    for(int i=0; i<2; ++i)
        copy[i] = str[i+2];

    month = convertStrToInt(copy);

    for(int i=0; i<4; ++i)
        copy[i] = str[i+4];

    year = convertStrToInt(copy);
}

char* Date::convertDateIntToDateStr() const
{
    char *date = new char[20];
    memset(date, 0, 20*sizeof(char));

    int p = 3;
    int year = this->year;
    int month = this->month;
    int day = this->day;

    while(year)
    {
        date[p--] = year%10 + '0';
        year /= 10;
    }

    if(p >= 0)
        for(int i=p; i>=0; --i)
            date[i] = '0';
    
    p = 5;

    while(month)
    {
        date[p--] = month%10 + '0';
        month /= 10;
    }

    if(p >= 4)
        for(int i=p; i>=4; --i)
            date[i] = '0';
    
    p = 7;
    
    while(day)
    {
        date[p--] = day%10 + '0';
        day /= 10;
    }

    if(p >= 6)
        for(int i=p; i>=6; --i)
            date[i] = '0';
    
    return date;
}
