#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "bucket.h"

Bucket::Bucket(int length, int type)
{
    records = new Record* [length];
    keys = new char* [length];
    
    for(int i=0; i<length; ++i)
    {
        records[i] = new Record(length, type);
        keys[i] = NULL;
    }

    next = NULL;
    pos = 0;
    this->length = length;
    this->type = type;
}

Bucket::~Bucket()
{
    delete next;

    for(int i=0; i<length; ++i)
    {
        delete records[i];
        delete[] keys[i];
    }
    
    delete[] records;
    delete[] keys;
}

void Bucket::insert(Info *new_info, const char *key)
{
    Bucket *node = this;

    while(1)
    {
        for(int i=0; i<node->pos; ++i)
            if(!strcmp(node->keys[i], key))
            {
                node->records[i]->insert(new_info);
                return;
            }
        
        if(node->pos < node->length)
        {
            node->keys[node->pos] = new char[strlen(key) + 1];
            strcpy(node->keys[node->pos], key);
            
            node->records[node->pos]->insert(new_info);
            ++node->pos;
            
            break;
        }

        if(node->next != NULL)
            node = node->next;
        else
        {
            node->next = new Bucket(length, type);
            node->next->insert(new_info, key);

            break;
        }
    }
}

void Bucket::deletes(const char *cdr, const char *caller)
{
    Bucket *node = this;

    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
            if(!strcmp(node->keys[i], caller))
            {
                node->records[i]->deletes(cdr);
                return;
            }

        node = node->next;
    }
}

void Bucket::find(const char *caller, const char *time1, const char *date1, const char *time2, const char *date2, List *li, int flag) const
{
    const Bucket *node = this;
    
    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
        {
            if(!strcmp(node->keys[i], caller))
            {
                node->records[i]->find(time1, date1, time2, date2, li, flag);
                return;
            }
        }

        node = node->next;
    }
}

void Bucket::topDest(const char *caller) const
{
    const Bucket *node = this;

    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
            if(!strcmp(node->keys[i], caller))
            {
                node->records[i]->topDest();
                return;
            }

        node = node->next;
    }
}

bool Bucket::exist(const char *caller, const char *callee) const
{
    const Bucket *node = this;

    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
            if(!strcmp(node->keys[i], caller))
                return node->records[i]->exist(callee);

        node = node->next;
    }

    return false;
}

void Bucket::printByPos(int ind) const
{
    const Bucket *node = this;

    while(node->next != NULL  &&  ind >= node->length)
    {
        node = node->next;
        ind -= node->length;
    }
    
    node->records[ind]->printRecords();
}

void Bucket::printBuckets(FILE *file) const
{
    const Bucket *node = this;

    while(1)
    {
        for(int i=0; i<node->pos; ++i)
            node->records[i]->printRecords(file);

        if(node->pos == node->length  &&  node->next != NULL)
            node = node->next;
        else
            break;
    }
}

void Bucket::clear()
{
    for(int i=0; i<length; ++i)
    {    
        records[i]->clear();

        delete[] keys[i];
        keys[i] = NULL;
    }

    pos = 0;
    
    delete next;
    next = NULL;
}
