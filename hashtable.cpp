#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "hashtable.h"

#define PRIME 8737
#define MOD 105943

HashTable::HashTable(int length, int bytes, int type)
{
    table = new Bucket* [length];
    
    for(int i=0; i<length; ++i)
        table[i] = new Bucket(bytes/sizeof(Bucket), type);
    
    this->length = length;
}

HashTable::~HashTable()
{
    for(int i=0; i<length; ++i)
        delete table[i];
    
    delete[] table;
}

void HashTable::insert(Info *new_info, const char *key)
{
    int hash_pos = hashFunction(key);
    table[hash_pos]->insert(new_info, key);
}

void HashTable::deletes(const char *cdr, const char *caller)
{
    int hash_pos = hashFunction(caller);
    table[hash_pos]->deletes(cdr, caller);
}

void HashTable::find(const char *caller, const char *time1, const char *date1, const char *time2, const char *date2, List *li, int flag) const
{
    int hash_pos = hashFunction(caller);
    table[hash_pos]->find(caller, time1, date1, time2, date2, li, flag);
}

void HashTable::topDest(const char *caller) const
{
    int hash_pos = hashFunction(caller);
    table[hash_pos]->topDest(caller);
}

bool HashTable::exist(const char *caller, const char *callee) const
{
    int hash_pos = hashFunction(caller);
    return table[hash_pos]->exist(caller, callee);
}

int HashTable::hashFunction(const char *key) const
{
    int hash = 0;

    for(int i=0; i<strlen(key); ++i)
    {
        hash = (hash*PRIME + (int)key[i]) % MOD;
        hash %= length;
    }

    return hash;
}

void HashTable::printByPos(int ind, FILE *file) const
{
    table[ind]->printBuckets(file);
}

void HashTable::printHashTable(FILE *file) const
{
    for(int i=0; i<length; ++i)
    {
        fprintf(file, "\n ---------- Bucket %d ---------- \n", i);
        printByPos(i, file);
    }
}

void HashTable::clear()
{
    for(int i=0; i<length; ++i)
        table[i]->clear();
}
