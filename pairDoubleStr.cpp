#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "pairDoubleStr.h"

PairDoubleStr::PairDoubleStr()
{
    val = 0;
    word = NULL;
}

PairDoubleStr::~PairDoubleStr()
{
    delete[] word;
}

void PairDoubleStr::insert(double val, char *word)
{
    this->val = val;

    this->word = new char[strlen(word) + 1];
    strcpy(this->word, word);
}

double PairDoubleStr::getDouble() const
{
    return val;
}

char* PairDoubleStr::getStr() const
{
    return word; 
}
