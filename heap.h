#ifndef HEAP_H
#define HEAP_H

#include "node.h"
#include "pairDoubleStr.h"

class Heap
{
    private:
        Node *head;
        int length;

        Node* addNode(double, char*);
        PairDoubleStr deleteLastNode();
    
    public:
        Heap();
        ~Heap();
        void insert(double, char*);
        Node* update(Node*, double, char*);
        Node* getTop() const;
        void popTop();
        void topPercent(int);
};

#endif
