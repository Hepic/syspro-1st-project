#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "node.h"

Node::Node(int type)
{
    left = right = parent = NULL;
    value = -1;
    phone = NULL;
    this->type = type;
}

Node::~Node()
{
    if(type == 1)
    {
        delete left;
        delete right;
    }

    else if(type == 2)
    {
        delete right;
    }

    delete[] phone;
}

double Node::getValue() const
{
    return value;
}

char* Node::getPhone() const
{
    return phone;
}

Node* Node::getLeft() const
{
    return left;
}

Node* Node::getRight() const
{
    return right;
}

Node* Node::getParent() const
{
    return parent;
}

void Node::insert(double value, char *phone)
{
    this->value = value;
    
    if(this->phone != NULL)
    {
        delete[] this->phone;
        this->phone = NULL;
    }

    this->phone = new char[strlen(phone) + 1];
    strcpy(this->phone, phone);
}

void Node::sets(double value, char *phone)
{
    this->value = value;
    this->phone = phone;
}

void Node::setLeft(Node *node)
{
    left = node;
}

void Node::setRight(Node *node)
{
    right = node;
}

void Node::setParent(Node *node)
{
    parent = node;
}

void Node::addValue(double value)
{
    this->value += value;
}
