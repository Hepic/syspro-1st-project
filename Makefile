CC = g++
FLAGS = -g -c
OUT = run
OBJS = main.o hashtable.o bucket.o record.o info.o date.o time.o heap.o node.o help_functions.o pairDoubleStr.o list.o

all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT)

main.o: main.cpp
	$(CC) $(FLAGS) main.cpp

hashtable.o: hashtable.cpp
	$(CC) $(FLAGS) hashtable.cpp

bucket.o: bucket.cpp
	$(CC) $(FLAGS) bucket.cpp

record.o: record.cpp
	$(CC) $(FLAGS) record.cpp

info.o: info.cpp
	$(CC) $(FLAGS) info.cpp

date.o: date.cpp
	$(CC) $(FLAGS) date.cpp

time.o: time.cpp
	$(CC) $(FLAGS) time.cpp

heap.o: heap.cpp
	$(CC) $(FLAGS) heap.cpp

node.o: node.cpp
	$(CC) $(FLAGS) node.cpp

help_functions.o: help_functions.cpp
	$(CC) $(FLAGS) help_functions.cpp

pairDoubleStr.o: pairDoubleStr.cpp
	$(CC) $(FLAGS) pairDoubleStr.cpp

list.o: list.cpp
	$(CC) $(FLAGS) list.cpp

clean:
	rm -f $(OBJS) $(OUT)
