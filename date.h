#ifndef DATE_H
#define DATE_H

class Date
{
    private:
        int day, month, year;

    public:
        int getDay() const;
        int getMonth() const;
        int getYear() const;
        void insert(const char*);
        char* convertDateIntToDateStr() const;
};

#endif
