#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "time.h"
#include "help_functions.h"

int Time::getHour() const
{
    return hour;
}

int Time::getMinute() const
{
    return minute;
}

void Time::insert(const char *str)
{
    char copy[4];
    memset(copy, 0, sizeof(copy));
    
    for(int i=0; i<2; ++i)
        copy[i] = str[i];
    
    hour = convertStrToInt(copy);
    
    for(int i=0; i<2; ++i)
        copy[i] = str[i+3];

    minute = convertStrToInt(copy);
}

char* Time::convertTimeIntToTimeStr() const
{
    char *time = new char[10];
    memset(time, 0, 10*sizeof(char));

    int p = 1;
    int hour = this->hour;
    int minute = this->minute;

    while(hour)
    {
        time[p--] = hour%10 + '0';
        hour /= 10;
    }

    if(p >= 0)
        for(int i=p; i>=0; --i)
            time[i] = '0';
    
    p = 4;

    while(minute)
    {
        time[p--] = minute%10 + '0';
        minute /= 10;
    }

    if(p >= 3)
        for(int i=p; i>=3; --i)
            time[i] = '0';
    
    time[2] = ':';

    return time;
}
