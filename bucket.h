#ifndef BUCKET_H
#define BUCKET_H

#include "record.h"

class Bucket
{
    private:
        Record **records;
        Bucket *next;
        char **keys;
        int pos, length, type;

    public:
        Bucket(int, int);
        ~Bucket();
        void insert(Info*, const char*);
        void deletes(const char*, const char*);
        void find(const char*, const char*, const char*, const char*, const char*, List *li = NULL, int flag = -1) const;
        void topDest(const char*) const;
        bool exist(const char*, const char*) const;
        void printByPos(int) const;
        void printBuckets(FILE *file = stdout) const;
        void clear();
};

#endif
