#ifndef HASHTABLE_H
#define HASHTABLE_H

#include "bucket.h"

class HashTable
{
    private:
        Bucket **table;
        int length;

    public:
        HashTable(int, int, int);
        ~HashTable();
        void insert(Info*, const char*);
        void deletes(const char*, const char*);
        void find(const char*, const char*, const char*, const char*, const char*, List *li = NULL, int flag = -1) const;
        void topDest(const char*) const;
        bool exist(const char*, const char*) const;
        int hashFunction(const char*) const;
        void printByPos(int, FILE *file = stdout) const;
        void printHashTable(FILE *file = stdout) const;
        void clear();
};

#endif
