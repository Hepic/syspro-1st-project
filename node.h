#ifndef NODE_H
#define NODE_H

class Node
{
    private:
        Node *left, *right, *parent;
        char *phone;
        double value;
        int type;

    public:
        Node(int);
        ~Node();
        Node* getLeft() const;
        Node* getRight() const;
        Node* getParent() const;
        double getValue() const;
        char* getPhone() const;
        void insert(double, char*);
        void sets(double, char*);
        void setLeft(Node*);
        void setRight(Node*);
        void setParent(Node*);
        void addValue(double);
};

#endif
