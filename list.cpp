#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "list.h"
#include "hashtable.h"

List::List()
{
    head = tail = NULL;
}

List::~List()
{
    delete head;
}

void List::insert(char *phone)
{
    Node *new_node = new Node(2);
    new_node->insert(0, phone);

    if(head == NULL  &&  tail == NULL)
    {
        head = tail = new_node;
        return;
    }
    
    new_node->setLeft(tail);
    tail->setRight(new_node);

    tail = new_node;
}

void List::deleteKnown(const HashTable &callee) // remove phones that are connected
{
    Node *first = head, *second, *next, *prev;

    while(first != NULL)
    {
        second = first->getRight();
        bool found = false;

        while(second != NULL)
        {
            if(callee.exist(first->getPhone(), second->getPhone())  ||  callee.exist(second->getPhone(), first->getPhone()))
            {
                next = second->getRight();
                prev = second->getLeft();
                
                if(next != NULL)
                    next->setLeft(prev);
                
                if(prev != NULL)
                    prev->setRight(next);
                
                second->setRight(NULL);
                delete second;
                second = next;
            
                found = true;  
            }
           
            else
                second = second->getRight();
        }
        
        if(found)
        {
            next = first->getRight();
            prev = first->getLeft();

            if(next != NULL)
                next->setLeft(prev);
        
            if(prev != NULL)
                prev->setRight(next);
            
            if(head == first)
            {
                head->setRight(NULL);
                delete head;
                head = first = next;
            }

            else
            {
                first->setRight(NULL);
                delete first;
                first = next;
            }
        }

        else
            first = first->getRight();
    }
}

bool List::exist(const char *phone) const
{
    Node *node = head;

    while(node != NULL)
    {
        if(!strcmp(node->getPhone(), phone))
            return true;

        node = node->getRight();
    }

    return false;
}

void List::print() const
{
    Node *node = head;    
    int cnt = 0;

    while(node != NULL)
    {
        ++cnt;
        printf("%s\n", node->getPhone());
        node = node->getRight();
    }

    if(!cnt)
        printf("No indist found\n");
}


void List::intersection(const List &li_1, const List &li_2)
{
    Node *node_1 = li_1.head;
    Node *node_2;

    while(node_1 != NULL)
    {
        node_2 = li_2.head;

        while(node_2 != NULL)
        {
            if(!strcmp(node_1->getPhone(), node_2->getPhone())) // add phones to this->list which exist in both of lists(li_1, li_2)
                insert(node_1->getPhone());

            node_2 = node_2->getRight();
        }

        node_1 = node_1->getRight();
    }
}
