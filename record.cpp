#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "record.h"

Record::Record(int length, int type)
{
    info = new Info* [length];

    for(int i=0; i<length; ++i)
        info[i] = NULL;

    next = NULL;
    pos = 0;
    this->length = length;
    this->type = type;
}

Record::~Record()
{
    delete next;

    for(int i=0; i<length; ++i)
    {
        if(type == 2) // only callee can delete info[i]
            delete info[i];
 
        info[i] = NULL;
    }

    delete[] info;
}

void Record::insert(Info *new_info)
{
    Record *node = this;
    
    while(node->pos == node->length  &&  node->next != NULL) // go to the next until find place for the new entry
        node = node->next;
        
    for(int i=0; i<node->pos; ++i)
        if(node->info[i] == NULL)
        {
            node->info[i] = new_info;
            return;
        } 

    if(node->pos < node->length)
    {
        node->info[node->pos] = new_info;
        ++node->pos; 
    }

    else
    {
        node->next = new Record(length, type);
        node->next->insert(new_info);
    }
}

void Record::deletes(const char *cdr)
{
    Record *node = this;
    
    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
            if(node->info[i] != NULL  &&  node->info[i]->equals(cdr))
            {
                Info *temp = getLast();

                if(node->info[i] == temp)
                    node->info[i] == NULL;
                else
                    node->info[i] = temp; // move last entry to the current position which is empty after deletion
                
                deleteLast();
                return;
            }

        node = node->next;
    }
}

Info* Record::getLast()
{
    Record *node = this;
    Record *prev = NULL;

    while(node->next != NULL)
    {
        prev = node;
        node = node->next;
    }
    
    return node->info[node->pos-1];
}

void Record::deleteLast()
{
    Record *node = this;
    Record *prev = NULL;

    while(node->next != NULL)
    {
        prev = node;
        node = node->next;
    }
    
    node->info[node->pos-1] = NULL;
    --node->pos;
    
    if(!node->pos  &&  prev != NULL)
    {
        delete prev->next; // if 'next' record is empty then delete it
        prev->next = NULL;
    }
}

void Record::find(const char *time1, const char *date1, const char *time2, const char *date2, List *li, int flag) const
{
    const Record *node = this;
    
    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
            if(node->info[i] != NULL  &&  node->info[i]->intoBounds(time1, date1, time2, date2))
            {
                if(li != NULL) // this is used in "indist"
                {
                    char *seq;
                    
                    if(flag == 1)
                        seq = node->info[i]->getDestination();
                    
                    else if(flag == 2)
                        seq = node->info[i]->getOriginator();
                    
                    if(!li->exist(seq))
                        li->insert(seq);
                    
                    delete[] seq;
                }

                else
                    node->info[i]->print();
            }

        node = node->next;
    }
}

void Record::topDest() const
{
    const Record *node = this;
    int counterDest[1010], visited[1010];
    int maxDest = -1;
    
    memset(counterDest, 0, sizeof(counterDest));

    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
        {
            int ret = node->info[i]->getDestInt();
            
            if(++counterDest[ret] > maxDest)
                maxDest = counterDest[ret]; // maxDest keeps the max frequency
        }
        
        node = node->next;
    }

    printf("Maximum number of dest: %d\n", maxDest);
    printf("Maxumum dests:\n");
    
    node = this;
    memset(visited, false, sizeof(visited)); // visited helps us to print each entry once

    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
        {
            int ret = node->info[i]->getDestInt();

            if(counterDest[ret] == maxDest  &&  !visited[ret]) // each entry that appears 'maxDest' times
            {
                char *temp = node->info[i]->getDestStr();
                
                for(int i=0; i<3; ++i)
                    printf("%c", temp[i]);
                printf("\n");

                delete[] temp;
                temp = NULL;
                
                visited[ret] = true;
            }
        }
        
        node = node->next;
    }
}

bool Record::exist(const char *phone) const
{
    const Record *node = this;
    
    while(node != NULL)
    {
        for(int i=0; i<node->pos; ++i)
        {
            char *dest = node->info[i]->getOriginator();

            if(!strcmp(dest, phone))
            {
                delete[] dest;
                return true;
            }

            delete[] dest;
        }

        node = node->next;
    }

    return false;
}

void Record::printByPos(int ind) const
{
    const Record *node = this;

    while(node->next != NULL  &&  ind >= node->length)
    {
        node = node->next;
        ind -= node->length;
    }
    
    if(node->info[ind] != NULL)
        node->info[ind]->print();
}

void Record::printRecords(FILE *file) const
{
    const Record *node = this;

    while(1)
    {
        for(int i=0; i<node->pos; ++i)
            if(node->info[i] != NULL)
                node->info[i]->print(file);

        if(node->pos == node->length  &&  node->next != NULL)
            node = node->next;
        else
            break;
    }
}

void Record::clear()
{
    for(int i=0; i<length; ++i)
    {
        if(type == 2) // only callee can delete info[i]
            delete info[i];

        info[i] = NULL;
    }
    
    pos = 0; 

    delete next;
    next = NULL;
}
